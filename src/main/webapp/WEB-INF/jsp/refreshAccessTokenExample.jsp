<%--Copyright (c) 2015. Healthcare Services Platform Consortium. All Rights Reserved.--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 5px;
            text-align: left;
        }
    </style>
    <title>Java Client Example - Standalone Clinician Confidential Webapp</title>
</head>
<body>
<h2></h2>

<h2>RefreshToken</h2>

<pre>${newAccessToken}</pre>

<br/>

</body>
</html>